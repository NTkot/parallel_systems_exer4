#ifndef VP_TREE_SEQ_H
#define VP_TREE_SEQ_H

#include <stdint.h>
#include "data_structs.h"

// Struct holding CUDA thread arguments (for thread_work_before_threshold and thread_work_after_threshold functions)
typedef struct {
    float *d_coords;
    tree_node *tree;
    int *indices_array;
    float *current_dists;
    int d;
    int current_depth;
    int current_threads_per_node;
} thread_args;

// Construct VP-tree data structure using CUDA implementation
__host__ void vp_tree_construct(float *d_coords, const int n, const int d, const int total_nodes, const int depth, tree_node *tree_out, int *indices_array_out);

// CUDA kernel function to perform work when the maximum number of threads in each block is bigger than the current depth nodes
__global__ void thread_work_before_threshold(thread_args args);

// CUDA kernel function to perform work when the maximum number of threads in each block is smaller than or equal to the current depth nodes
__global__ void thread_work_after_threshold(thread_args args);

// Find kth_smallest element in array using quickselect
__device__ float kth_smallest(float *a, int *a_idx, int left, int right, int k);

// Function used for partitioning by kth_smallest function
__device__ int partition(float *arr, int *arr_idx, int low, int high);

__host__ float kth_smallest_host(float *a, int *a_idx, int left, int right, int k);
__host__ int partition_host(float *arr, int *arr_idx, int low, int high);

// Memory swap float pointer values for GPU variables
__device__ void swap_float_gpu(float *a, float *b);

// Memory swap int pointer values for GPU variables
__device__ void swap_int_gpu(int *a, int *b);

#endif