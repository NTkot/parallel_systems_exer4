#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "data_structs.h"
#include "vp_tree.cuh"
#include "utils.cuh"

__host__ void vp_tree_construct(float *d_coords, const int n, const int d, const int total_nodes, const int depth, tree_node *tree_out, int *indices_array_out) {
    int max_threads_per_block;
    cudaDeviceProp prop;
    cudaError_t cudaError;

    // CUDA variables holding tree structure (tree) and global indices_array
    tree_node *tree;
    int *d_indices_array;
    float *d_current_dists;
    float *current_dists;

    // Get maxThreadsPerBlock from GPU device
    cudaGetDeviceProperties(&prop, 0);
    max_threads_per_block = prop.maxThreadsPerBlock;

    // Allocate memory for tree node structure, indices array and current distances array
    cudaMalloc((void **)&d_current_dists, n * sizeof(float));
    cudaMalloc((void **)&tree,          total_nodes * sizeof(tree_node));
    cudaMalloc((void **)&d_indices_array, n * sizeof(int));

    // Allocate memory for current_dists
    current_dists = (float *)malloc(n * sizeof(float));

    // Initialize variables and construct tree structure
    tree_out[0].points_count     = n;
    tree_out[0].first_idx_offset = 0;
    for(int i = 0; i < n; i++)
        indices_array_out[i] = i;
    for(int ii = 1; ii < depth; ii++) {
        int current_depth_nodes    = (1 << ii);
        int current_first_node_idx = current_depth_nodes - 1;
        for(int i = current_first_node_idx; i < (2*current_first_node_idx+1); i++) {
            int father_idx = i/2 - (1 - (i % 2));
            tree_out[i].points_count     = tree_out[father_idx].points_count/2 - !((i % 2) || (tree_out[father_idx].points_count % 2));
            tree_out[i].first_idx_offset = tree_out[father_idx].first_idx_offset + !(i % 2) * tree_out[i-1].points_count;
            tree_out[i].median_dist = 0.0;
        }
    }
    // Move values into device(GPU) variables
    cudaMemcpy(tree, tree_out, total_nodes * sizeof(tree_node), cudaMemcpyHostToDevice);
    cudaMemcpy(d_indices_array, indices_array_out, n * sizeof(int), cudaMemcpyHostToDevice);

    // VP-tree construction section
    // Find threshold_depth: the depth of the tree in which the current nodes in this depth are equal to or greater than the maxThreadsPerBlock CUDA variable
    int threshold_depth = 0;
    int current_depth_nodes, current_blocks;
    while((1 << threshold_depth) < max_threads_per_block)
        threshold_depth++;

    // Prepare args for kernel functions
    thread_args cuda_thread_args;   
    cuda_thread_args.d_coords      = d_coords;
    cuda_thread_args.tree          = tree;
    cuda_thread_args.indices_array = d_indices_array;
    cuda_thread_args.current_dists = d_current_dists;
    cuda_thread_args.d = d;

    // Up to threshold_depth, current_depth_nodes < max_threads_per_block
    for(int ii = 0; (ii < threshold_depth) && (ii < depth); ii++) {
        current_depth_nodes = (1 << ii);
        current_blocks      = 1;
        cuda_thread_args.current_depth            = ii;
        cuda_thread_args.current_threads_per_node = max_threads_per_block / current_depth_nodes;
        thread_work_before_threshold <<<current_blocks, max_threads_per_block>>> (cuda_thread_args);

        cudaError = cudaDeviceSynchronize();
        if (cudaError != cudaSuccess)
            printf("Depth = %d: Kernel launch failed with error \"%s\".\n", ii, cudaGetErrorString(cudaError));

        // Run quickselect on CPU instead of GPU (CPU is faster with high number of points) 
        cudaMemcpy(indices_array_out, d_indices_array, n * sizeof(int), cudaMemcpyDeviceToHost);
        cudaMemcpy(current_dists, d_current_dists, n * sizeof(float), cudaMemcpyDeviceToHost);
        for(int jj = 0; jj < current_depth_nodes; jj++) {
            int node_idx         = (1 << ii) - 1 + jj;
            int current_n        = tree_out[node_idx].points_count;
            int first_idx_offset = tree_out[node_idx].first_idx_offset;

            float median_dist = kth_smallest_host(&current_dists[first_idx_offset], &indices_array_out[first_idx_offset], 0, current_n-2, current_n/2 + (current_n % 2));

            cudaMemcpy(&tree[node_idx].median_dist, &median_dist, sizeof(float), cudaMemcpyHostToDevice);
        }
        // Write results of quickselect back to GPU variable so data can be used in the iteration
        cudaMemcpy(d_indices_array, indices_array_out, n * sizeof(int), cudaMemcpyHostToDevice);
    }
    // From this point onwards, current_depth_nodes >= max_threads_per_block
    for(int ii = threshold_depth; ii < depth; ii++) {
        current_depth_nodes = (1 << ii);
        current_blocks      = current_depth_nodes / max_threads_per_block;
        cuda_thread_args.current_depth            = ii;
        cuda_thread_args.current_threads_per_node = 1;
        thread_work_after_threshold <<<current_blocks, max_threads_per_block>>> (cuda_thread_args);

        cudaError = cudaDeviceSynchronize();
        if (cudaError != cudaSuccess)
            printf("Depth = %d: Kernel launch failed with error \"%s\".\n", ii, cudaGetErrorString(cudaError));
    }

    // Transfer files back to host variables
    cudaMemcpy(tree_out, tree, total_nodes * sizeof(tree_node), cudaMemcpyDeviceToHost);
    cudaMemcpy(indices_array_out, d_indices_array, n * sizeof(int), cudaMemcpyDeviceToHost);

    // Free device allocated memory
    cudaFree(tree);
    cudaFree(d_current_dists);
    cudaFree(d_indices_array);
    free(current_dists);
}

__global__ void thread_work_before_threshold(thread_args args) {
    tree_node *tree              = args.tree;
    int *indices_array           = args.indices_array;
    float *coords                = args.d_coords;
    float *current_dists         = args.current_dists;
    int current_threads_per_node = args.current_threads_per_node;
    int current_depth            = args.current_depth;
    int d = args.d;

    int node_idx         = (1 << current_depth) - 1 + threadIdx.x / current_threads_per_node; // Current node index in the global tree struct array
    int current_n        = tree[node_idx].points_count;
    int first_idx_offset = tree[node_idx].first_idx_offset;
    int local_id         = threadIdx.x % current_threads_per_node;    // Shows the id within the group of threads that belong to the same tree node

    // The first thread of the group belonging to the same tree node will assign the vantage point to the tree_node struct array
    if(local_id == 0 && current_n > 0)
        tree[node_idx].vp_idx = indices_array[first_idx_offset + current_n - 1];
    // If one or none points belong to the node, no point in continuing
    if(current_n <= 1)
        return;

    // Find all distances between the vantage point and the rest of the points belonging to the node
    int vp_idx = indices_array[first_idx_offset + current_n - 1];
    for(int i = local_id; i < current_n; i+=current_threads_per_node) {
        int current_idx = indices_array[first_idx_offset + i];
        float sum = 0;
        for(int j = 0; j < d; j++)
            sum += (coords[current_idx*d + j] - coords[vp_idx*d + j]) * (coords[current_idx*d + j] - coords[vp_idx*d + j]);
        current_dists[first_idx_offset + i] = sum;
    }
}

__global__ void thread_work_after_threshold(thread_args args) {
    tree_node *tree              = args.tree;
    int *indices_array           = args.indices_array;
    float *coords                = args.d_coords;
    float *current_dists         = args.current_dists;
    int current_threads_per_node = args.current_threads_per_node;
    int current_depth            = args.current_depth;
    int d = args.d;

    int node_idx         = (1 << current_depth) - 1 + blockIdx.x * blockDim.x + threadIdx.x;    // Current node index in the global tree struct array
    int current_n        = tree[node_idx].points_count;
    int first_idx_offset = tree[node_idx].first_idx_offset;

    // Each thread is assigned to a specific tree node, therefore each thread assigns to each corresponding tree node the vantage point index (if current_n > 0)
    if(current_n > 0)
        tree[node_idx].vp_idx = indices_array[first_idx_offset + current_n - 1];
    // If one or none points belong to the node, no point in continuing with finding distance between vantage point and rest of points
    if(current_n <= 1)
        return;

    // Find all distances between the vantage point and the rest of the points belonging to the node
    int vp_idx = tree[node_idx].vp_idx;
    for(int i = 0; i < current_n; i++) {
        int current_idx = indices_array[first_idx_offset + i];
        float sum = 0;
        for(int j = 0; j < d; j++)
            sum += (coords[current_idx*d + j] - coords[vp_idx*d + j]) * (coords[current_idx*d + j] - coords[vp_idx*d + j]);
        current_dists[first_idx_offset + i] = sum;
    }

    // Each thread works on a different tree node and therefore no need for __syncthreads(), since running quickselect will not create a race condition

    // Each thread is assigned to a specific tree node, therefore each thread runs individually distance-based quickselect
    tree[node_idx].median_dist = kth_smallest(&current_dists[first_idx_offset], &indices_array[first_idx_offset], 0, current_n-2, current_n/2 + (current_n % 2));
}

// If you want to find median from odd  number sized vector, k = size/2 + 1
// If you want to find median from even number sized vector, k = size/2 + 1 && k = size/2 and then median = average of these two
__device__ float kth_smallest(float *a, int *a_idx, int left, int right, int k) {
    int pivotIndex;
    while (left <= right) {
        pivotIndex = partition(a, a_idx, left, right);
 
        if (pivotIndex == (k - 1))
            return a[pivotIndex];
        else if (pivotIndex < (k - 1))
            left = pivotIndex + 1;
        else
            right = pivotIndex - 1;
    }
    return -1;
}

__device__ int partition(float *arr, int *arr_idx, int low, int high) {
    unsigned int i = low;
    float pivot = arr[high];
    for (unsigned int j = low; j < high; j++) {
        if(arr[j] < pivot) {
            swap_float_gpu(&arr[i], &arr[j]);
            swap_int_gpu(&arr_idx[i], &arr_idx[j]);
            i++;
        }
    }

    swap_float_gpu(&arr[i], &arr[high]);
    swap_int_gpu(&arr_idx[i], &arr_idx[high]);
    return i;
}

__host__ float kth_smallest_host(float *a, int *a_idx, int left, int right, int k) {
    int pivotIndex;
    while (left <= right) {
        pivotIndex = partition_host(a, a_idx, left, right);
 
        if (pivotIndex == (k - 1))
            return a[pivotIndex];
        else if (pivotIndex < (k - 1))
            left = pivotIndex + 1;
        else
            right = pivotIndex - 1;
    }
    return -1;
}

__host__ int partition_host(float *arr, int *arr_idx, int low, int high) {
    unsigned int i = low;
    float pivot = arr[high];
    for (unsigned int j = low; j < high; j++) {
        if(arr[j] < pivot) {
            swap_float(&arr[i], &arr[j]);
            swap_int(&arr_idx[i], &arr_idx[j]);
            i++;
        }
    }

    swap_float(&arr[i], &arr[high]);
    swap_int(&arr_idx[i], &arr_idx[high]);
    return i;
}

__device__ void swap_float_gpu(float *a, float *b) {
    float temp = *a;
    *a = *b;
    *b = temp;
}

__device__ void swap_int_gpu(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}