#ifndef HEAP_CUH
#define HEAP_CUH

typedef struct {
    int *array_idx;
    float *priorities;
    int size;
    int maxsize;
} heap;

__host__ int init_heap(heap *h, int size);
__host__ void heapify(heap *h, int i);
__host__ void insert_heap(heap *h, float new_priority, int new_idx);
__host__ void pop_heap(heap *h, float *priority, int *idx);
__host__ void noreturn_pop_heap(heap *h);
__host__ float current_max_heap(heap *h);

#endif