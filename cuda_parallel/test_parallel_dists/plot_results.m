x = dlmread('results.txt', " " )
z = x(:,3) - x(:,4);
color = zeros(size(z,1), 1);
pos_z = z(z > 0);
neg_z = z(z < 0);
color(z > 0) = pos_z / max(pos_z) * 0.25 + 0.75;
color(z < 0) = (abs(neg_z) / max(abs(neg_z))) * 0.25;
scatter3(x(:,1), x(:,2), z(:), 50, color, 'filled');
set(gca, 'yscale', 'log');
xlabel('Number of points');
ylabel('Number of dimensions');
zlabel('Difference between CPU and GPU (ms)');