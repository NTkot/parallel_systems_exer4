#pragma once

#define COORD_SEED time(NULL)
#define MAX_COORD  2.0
#define MIN_COORD -2.0
#define STEP 32
#define REPETITIONS 10

typedef struct {
    int      n, d;
    float   *coords;
} points_struct;

int number_of_points[10] = {STEP, 2*STEP, 3*STEP, 4*STEP, 5*STEP, 6*STEP, 7*STEP, 8*STEP, 9*STEP, 10*STEP};
int number_of_dims[7]    = {2, 4, 8, 16, 32, 64, 128};

// int number_of_points[1] = {1600};
// int number_of_dims[1] = {32};

__host__ int init_points(points_struct *points);
__global__ void distance_gpu(const float* coords, const float *query_coords, const int n, const int d, float *dists);