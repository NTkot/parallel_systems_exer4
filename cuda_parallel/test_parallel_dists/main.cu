#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include "main.cuh"

int main() {
    points_struct points;
    struct timeval start, end;
    cudaEvent_t cuda_start, cuda_stop;
    cudaError_t cudaError;
    float *current_dists, *cuda_current_dists, *d_current_dists, *d_coords, *d_query_coords;
    float cpu_elapsed_time, cpu_elapsed_total_time, cuda_elapsed_time, cuda_elapsed_total_time;
    int query_idx;
    FILE *file;

    // File to write results
    file = fopen("results.txt", "w");
    if(file == NULL) {
        printf("Could not open file\n");
        return -1;
    }

    for(int ii = 0; ii < (sizeof(number_of_points)/sizeof(int)); ii++) {
        for(int jj = 0; jj < (sizeof(number_of_dims)/sizeof(int)); jj++) {
            points.n = number_of_points[ii];
            points.d = number_of_dims[jj];
            init_points(&points);
            query_idx = points.n-1;
            cudaMalloc((void**)&d_coords, points.n * points.d * sizeof(float));
            cudaMemcpy(d_coords, points.coords, points.n * points.d * sizeof(float), cudaMemcpyHostToDevice);
            
            cpu_elapsed_total_time  = 0;
            cuda_elapsed_total_time = 0;

            for(int kk = 0; kk < REPETITIONS; kk++) {
                current_dists = (float *)calloc(points.n, sizeof(float));
                cuda_current_dists = (float *)malloc(points.n * sizeof(float));

                // CPU
                gettimeofday(&start, NULL); // Start counting
                
                #pragma omp parallel for num_threads(3)
                for(int i = 0; i < points.n; i++) {
                    for(int k = 0; k < points.d; k++) {
                        current_dists[i] += (points.coords[query_idx*points.d + k] - points.coords[i*points.d + k]) *
                                            (points.coords[query_idx*points.d + k] - points.coords[i*points.d + k]);
                    }
                }
                gettimeofday(&end, NULL);   // End counting
                cpu_elapsed_time = (end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec); // Elapsed time counted in us


                // GPU
                cudaEventCreate(&cuda_start);
                cudaEventRecord(cuda_start, 0);
 
                cudaMalloc((void**)&d_current_dists, points.n * sizeof(float));
                cudaMalloc((void**)&d_query_coords,  points.d * sizeof(float));
                cudaMemcpy(d_query_coords, &points.coords[query_idx*points.d], points.d * sizeof(float), cudaMemcpyHostToDevice);
                distance_gpu <<<1, 1024, points.d*sizeof(float) + 1024*sizeof(float)>>>(d_coords, d_query_coords, points.n, points.d, d_current_dists);
                cudaMemcpy(cuda_current_dists, d_current_dists, points.n * sizeof(float), cudaMemcpyDeviceToHost);
                
                cudaEventCreate(&cuda_stop);
                cudaEventRecord(cuda_stop, 0);
                cudaEventSynchronize(cuda_stop);
                cudaEventElapsedTime(&cuda_elapsed_time, cuda_start, cuda_stop);
                
                cudaError = cudaGetLastError();
                if(cudaError != 0) {
                    printf("CUDA ERROR %d\n", cudaError);
                }


                // Check outputs
                for(int i = 0; i < points.n; i++) {
                    if(((current_dists[i] - cuda_current_dists[i]) * (current_dists[i] - cuda_current_dists[i])) > 1E-15f) {
                        printf("ERROR: Distances in CPU and GPU do not match (ii = %d, jj = %d)\nDiff^2 = %.3f\n\n", ii, jj,((current_dists[i] - cuda_current_dists[i]) * (current_dists[i] - cuda_current_dists[i])));
                        continue;
                    }
                }


                // Add elapsed time to total times
                cpu_elapsed_total_time  += ((float)cpu_elapsed_time);
                cuda_elapsed_total_time += cuda_elapsed_time;

                // Print average times
                // printf("CPU-Sequential elapsed time = %f us\n", (float)cpu_elapsed_time);
                // printf("CUDA elapsed time           = %f us\n", 1000*cuda_elapsed_time);
            }
            cpu_elapsed_total_time  /= REPETITIONS;
            cuda_elapsed_total_time /= REPETITIONS;

            // Print average times
            // printf("AVG: CPU-Sequential elapsed time = %f ms\n", (float)cpu_elapsed_total_time/1000);
            // printf("AVG: CUDA elapsed time           = %f ms\n", cuda_elapsed_total_time);
            fprintf(file,"%d %d %.3f %.3f\n", points.n, points.d, ((float)cpu_elapsed_total_time)/1000, cuda_elapsed_total_time);
        }
    }
    fclose(file);
}

__host__ int init_points(points_struct *points) {
    size_t n, d;
    n = (size_t)points->n;
    d = (size_t)points->d;

    points->coords = (float *)malloc(n * d * sizeof(float));
    if(points->coords == NULL) {
        perror("points->coords");
        return -1;
    }

    srand(COORD_SEED);
    for(int i = 0; i < n*d; i++)
        points->coords[i] = (MAX_COORD - MIN_COORD)*((float)rand() / (float)RAND_MAX) + MIN_COORD;

    return 1;
}

__global__ void distance_gpu(const float* coords, const float *query_coords, const int n, const int d, float *dists) {
    extern __shared__ float shared[];
    float *shared_query_coords = shared;
    float *curr_dist = &shared[d];

    for(int jj = threadIdx.x; jj < d; jj += 1024)
        shared_query_coords[jj] = query_coords[jj];
    __syncthreads();

    for(int ii = threadIdx.x; ii < n; ii += 1024) {
        curr_dist[threadIdx.x] = 0;
        for(int j = 0; j < d; j+=2) {
            curr_dist[threadIdx.x] += (coords[ii*d + j] - shared_query_coords[j]) * (coords[ii*d + j] - shared_query_coords[j]);
            curr_dist[threadIdx.x] += (coords[ii*d + j + 1] - shared_query_coords[j + 1]) * (coords[ii*d + j + 1] - shared_query_coords[j + 1]);
        }
        dists[ii] = curr_dist[threadIdx.x];
    }
}