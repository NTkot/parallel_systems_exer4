#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include "data_structs.h"
#include "utils.cuh"

#define MAX_COORD   5
#define MIN_COORD  -5
#define COORD_SEED 2

__host__ int init_points(points_struct *points) {
    size_t n, d;
    n = (size_t)points->n;
    d = (size_t)points->d;

    points->coords    = (float *)malloc(n * d * sizeof(float));
    if(points->coords == NULL) {
        perror("points->coords");
        return -1;
    }
    srand(COORD_SEED);
    for(int i = 0; i < n*d; i++)
        points->coords[i] = (MAX_COORD - MIN_COORD)*((float)rand() / (float)RAND_MAX) + MIN_COORD;

    return 1;
}

__host__ void swap_float(float *a, float *b) {
    float temp = *a;
    *a = *b;
    *b = temp;
}

__host__ void swap_int(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

__host__ void print_int_vector(int *vec, int n) {
    for(int i = 0; i < n; i++)
        printf("%d ", vec[i]);
    printf("\n");
}

__host__ void print_float_vector(float *vec, int n) {
    for(int i = 0; i < n; i++)
        printf("%.3f ", vec[i]);
    printf("\n");
}

__host__ void print_2d_float_array(float *array, int n, int d) {
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < d; j++)
            printf("%.3f ", array[i*d + j]);
        printf("\n");
    }
}