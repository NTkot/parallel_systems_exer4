#ifndef DATA_STRUCTS_H
#define DATA_STRUCTS_H

#include <stdint.h>

typedef struct {

    /**
     * @struct tree_node
     * @brief Structure containing info about a VP tree node
     * @var tree_node::vp_idx
     * Member 'vp_idx' contains the index of the vantage point in the current node
     * @var tree_node::points_count
     * Member 'points_count' is the number of elements in the set of points belonging to the node
     * @var tree_node::first_idx_offset
     * Member 'first_idx_offset' contains the offset in the indices array used globally by the tree nodes in CUDA implementation
     * @var tree_node::median_dist
     * Member 'median_dist' is the median dist found between the vantage point and the rest of the points
     */

    int vp_idx;
    int points_count;
    int first_idx_offset;
    float median_dist;
} tree_node;

typedef struct {

    /**
     * @struct points_struct
     * @brief Structure holding information about points in space
     * @var points_struct::n
     * Member 'n' holds the number of points
     * @var points_struct::d
     * Member 'd' holds the number of dimensions
     * @var points_struct::coords
     * Member 'coords' holds the coordinates of all points
     */

    int      n, d;
    float   *coords;
} points_struct;

#endif
