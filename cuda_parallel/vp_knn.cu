#include <stdio.h>
#include <float.h>
#include <math.h>
#include "vp_knn.cuh"
#include "heap.cuh"
#include "data_structs.h"

__host__ void vp_knn(const tree_node *tree, const int total_nodes, const points_struct *points, const float *query_coords, const int k, float *dist_array, int *neighbors_idx) {
    float tau = FLT_MAX;
    heap neighbors;
    init_heap(&neighbors, k);
    for(int i = 0; i < k; i++)
        insert_heap(&neighbors, FLT_MAX, -1);

    search(tree, total_nodes, 0, points, query_coords, &neighbors, &tau);
    for(int i = (k-1); i >= 0; i--)
        pop_heap(&neighbors, &dist_array[i], &neighbors_idx[i]);

}

__host__ void search(const tree_node *tree, const int total_nodes, const int node_idx, const points_struct *points, const float *query_coords, heap *neighbors, float *tau) {
    if(tree[node_idx].points_count == 0)
        return;
    int vp_idx = tree[node_idx].vp_idx;

    // Find distance between query point and current vantage point
    float dist_sqr = 0;
    for(int l = 0; l < points->d; l++) {
        dist_sqr += (points->coords[vp_idx*points->d + l] - query_coords[l]) *
                    (points->coords[vp_idx*points->d + l] - query_coords[l]);
    }

    // If the distance found is smaller than the greatest distance in the heap, then pop root node from heap and insert vantage point to heap
    if(dist_sqr < *tau) {
        noreturn_pop_heap(neighbors);
        insert_heap(neighbors, dist_sqr, vp_idx);
        *tau = current_max_heap(neighbors);
    }
    
    // If we are at bottom level, no need to search in children since there are no childs
    if(node_idx >= total_nodes/2)
        return;
    
    float dist, median_dist_sqrt, tau_sqrt;
    dist             = sqrt(dist_sqr);
    median_dist_sqrt = sqrt(tree[node_idx].median_dist);
    tau_sqrt         = sqrt(*tau);
    // Check if query point lies within circle with vantage point as its center and
    // current median dist as radius (circle that contains points that are assigned to left child)
    if(dist_sqr < tree[node_idx].median_dist) {
        // Search left child first since query point lies within the circle mentioned in line 49
        search(tree, total_nodes, 2*node_idx+1, points, query_coords, neighbors, tau);
        
        // Check if the circle with query point as its center and tau_sqrt as radius does NOT lie entirely inside the circle mentioned in line 49
        if((dist + tau_sqrt) >= median_dist_sqrt)
            search(tree, total_nodes, 2*node_idx+2, points, query_coords, neighbors, tau);
    } else {
        // Search right child first since query point lies outside the circle mentioned in line 49
        search(tree, total_nodes, 2*node_idx+2, points, query_coords, neighbors, tau);

        // Check if the circle with query point as its center and tau_sqrt as radius does NOT lie entirely outside the circle mentioned in line 49
        if((dist - tau_sqrt) <= median_dist_sqrt)
            search(tree, total_nodes, 2*node_idx+1, points, query_coords, neighbors, tau);

    }
}

__host__ int test_knn(const tree_node *tree, const int total_tree_nodes, const points_struct *points, const float *query_coords, const int k, const float *knn_dists, const int *knn_idx) {
    heap neighbors_validated;
    init_heap(&neighbors_validated, k);
    for(int i = 0; i < k; i++)
        insert_heap(&neighbors_validated, FLT_MAX, -1);
    
    for(int i = 0; i < points->n; i++) {
        float dist_sqr = 0;
        for(int k = 0; k < points->d; k++)
            dist_sqr += (query_coords[k] - points->coords[i*points->d + k]) * (query_coords[k] - points->coords[i*points->d + k]);
        if(dist_sqr < current_max_heap(&neighbors_validated)) {
            noreturn_pop_heap(&neighbors_validated);
            insert_heap(&neighbors_validated, dist_sqr, i);
        }
    }

    float current_dist;
    int   current_idx, flag = 1;
    for(int i = (k-1); i >= 0; i--) {
        pop_heap(&neighbors_validated, &current_dist, &current_idx);
        if((current_idx != knn_idx[i]) && ((knn_dists[i] - current_dist) * (knn_dists[i] - current_dist) > 1E-15f)) {
            flag = 0;
            printf("Fail at (%d) neighbor\nIndices = %d  vs  %d\nDist^2  = %.3f  vs  %.3f\n", i, knn_idx[i], current_idx, knn_dists[i], current_dist);
        }
    }
    // if(flag == 1)
    //     printf("KNN RAN SUCCESSFULLY");
    // printf("\n\n");
    return flag;
}