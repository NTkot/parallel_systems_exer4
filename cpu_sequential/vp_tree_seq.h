#ifndef VP_TREE_SEQ_H
#define VP_TREE_SEQ_H

#include "data_structs.h"

// Construct VP-tree data structure sequentially
void vp_tree_construct(points_struct *points, tree_node *tree, int total_nodes);

// Find kth_smallest element in array using quickselect
float kth_smallest(float *a, int *a_idx, int left, int right, int k);

// Function used for partitioning by kth_smallest function
int partition(float *arr, int *arr_idx, int low, int high);

#endif