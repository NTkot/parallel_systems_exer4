#include <stdio.h>
#include <stdlib.h>
#include "heap.h"
#include "utils.h"

int init_heap(heap *h, int size) {
    h->array_idx  = malloc(size * sizeof(int));
    h->priorities = malloc(size * sizeof(float));
    if((h->priorities == NULL) || (h->array_idx == NULL))
        return 0;
    h->maxsize = size;
    h->size = 0;

    return 1;
}

void heapify(heap *h, int i) {
    if(h->size > 1) {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;
        if (l < h->size && h->priorities[l] > h->priorities[largest])
            largest = l;
        if (r < h->size && h->priorities[r] > h->priorities[largest])
            largest = r;
        if (largest != i) {
            swap_float(&h->priorities[i], &h->priorities[largest]);
            swap_int(&h->array_idx[i], &h->array_idx[largest]);
            heapify(h, largest);
        }
    }
}

void insert_heap(heap *h, float new_priority, int new_idx) {
    if(h->size == h->maxsize) {
        printf("ERROR: Tried to input object into full heap\n");
        exit(EXIT_FAILURE);
    }
    if (h->size == 0) {
        h->priorities[0] = new_priority;
        h->array_idx[0]  = new_idx;
        h->size++;
    } else {
        h->priorities[h->size] = new_priority;
        h->array_idx[h->size]  = new_idx;
        h->size++;
        for (int i = h->size / 2 - 1; i >= 0; i--)
            heapify(h, i);
    }
}

void pop_heap(heap *h, float *priority, int *idx) {
    if(h->size == 0) {
        printf("ERROR: Tried to pop object from empty heap\n");
        exit(EXIT_FAILURE);
    }
    *priority = h->priorities[0];
    *idx      = h->array_idx[0];
    swap_float(&h->priorities[0], &h->priorities[h->size - 1]);
    swap_int(&h->array_idx[0]   , &h->array_idx[h->size - 1]);
    h->size--;
    for (int i = h->size / 2 - 1; i >= 0; i--)
        heapify(h, i);
}

void noreturn_pop_heap(heap *h) {
    if(h->size == 0) {
        printf("ERROR: Tried to pop object from empty heap\n");
        exit(EXIT_FAILURE);
    }
    swap_float(&h->priorities[0], &h->priorities[h->size - 1]);
    swap_int(&h->array_idx[0]   , &h->array_idx[h->size - 1]);
    h->size--;
    for (int i = h->size / 2 - 1; i >= 0; i--)
        heapify(h, i);
}

float current_max_heap(heap *h) {
    if(h->size == 0) {
        printf("ERROR: Tried to get max priority from empty heap\n");
        exit(EXIT_FAILURE);
    }
    return h->priorities[0];
}