#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "data_structs.h"
#include "vp_tree_seq.h"
#include "utils.h"

void vp_tree_construct(points_struct *points, tree_node *tree, int total_nodes) {
    int n, d, current_n, current_vp;
    float *current_dists;
    n = points->n;
    d = points->d;

    // Starting conditions (Initialize root node)
    tree[0].points_count = n;
    tree[0].points_idx   = malloc(n * sizeof(int));
    for(int i = 0; i < n; i++)
        tree[0].points_idx[i] = i;

    // VP-tree construction
    for(int i = 0; i < total_nodes; i++) {
        // Get info about how many points are contained within the node
        current_n = tree[i].points_count;
        if(current_n > 0)
            tree[i].vp_idx = tree[i].points_idx[current_n - 1]; // Pick last point in set as the vantage point
        if(current_n <= 1)  // If one or none points are contained within the node, then no calculations are needed
            continue;
        
        // Construct current dists vector between points in node and vantage point
        current_vp     = tree[i].vp_idx;
        current_dists  = malloc((current_n-1) * sizeof(float));
        for(int k = 0; k < (current_n-1); k++) {
            float sum = 0;
            for(int l = 0; l < d; l++) {
                sum += (points->coords[current_vp*d + l] - points->coords[tree[i].points_idx[k]*d + l]) *
                       (points->coords[current_vp*d + l] - points->coords[tree[i].points_idx[k]*d + l]);
            }
            current_dists[k] = sum;
        }

        // Quick-select
        tree[i].median_dist = kth_smallest(current_dists, tree[i].points_idx, 0, current_n-2, current_n/2 + (current_n % 2));

        // Write inside/outside point info to two childs
        // Left child
        tree[2*i+1].points_count = current_n/2;
        tree[2*i+1].points_idx   = malloc(tree[2*i+1].points_count * sizeof(int));
        memcpy(tree[2*i+1].points_idx, tree[i].points_idx, tree[2*i+1].points_count * sizeof(int));

        // Right child
        tree[2*i+2].points_count = current_n/2 - (1 - (current_n % 2));
        if(tree[2*i+2].points_count != 0) {
            tree[2*i+2].points_idx   = malloc(tree[2*i+2].points_count * sizeof(int));
            memcpy(tree[2*i+2].points_idx, tree[i].points_idx + current_n/2, tree[2*i+2].points_count * sizeof(int));
        }
    }
}


// If you want to find median from odd  number sized vector, k = size/2 + 1
// If you want to find median from even number sized vector, k = size/2 + 1 && k = size/2 and then median = average of these two
float kth_smallest(float *a, int *a_idx, int left, int right, int k) {
    int pivotIndex;
    while (left <= right) {
        pivotIndex = partition(a, a_idx, left, right);
 
        if (pivotIndex == (k - 1))
            return a[pivotIndex];
        else if (pivotIndex < (k - 1))
            left = pivotIndex + 1;
        else
            right = pivotIndex - 1;
    }
    return -1;
}


int partition(float *arr, int *arr_idx, int low, int high) {
    unsigned int i = low;
    float pivot = arr[high];
    for (unsigned int j = low; j < high; j++) {
        if(arr[j] < pivot) {
            swap_float(&arr[i], &arr[j]);
            swap_int(&arr_idx[i], &arr_idx[j]);
            i++;
        }
    }

    swap_float(&arr[i], &arr[high]);
    swap_int(&arr_idx[i], &arr_idx[high]);
    return i;
}