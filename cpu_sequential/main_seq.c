#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "data_structs.h"
#include "utils.h"
#include "vp_tree_seq.h"
#include "vp_knn_seq.h"

// #define KNN_PROGRESS

int main(int argc, char **argv) {
    points_struct points;
    tree_node     *tree;
    int depth, total_nodes;
    struct timeval start, end;
    long elapsed_time;
    int run_vp_based_knn;

    // Hyperparameter inputs
    if(argc != 4) {
        printf("Usage:\n        ./main_seq <number_of_points> <number_of_dimensions> <run_vp_based_knn>\n\n");
        return -1;
    }
    points.n         = (atoi(argv[1]) >= 2) ? atoi(argv[1]) : 0;
    points.d         = (atoi(argv[2]) >= 0) ? atoi(argv[2]) : 0;
    run_vp_based_knn = (atoi(argv[3]) != 0) ? 1 : 0;
    if(points.n == 0 || points.d == 0) {
        printf("Invalid number of n or d\nn must be greater than 1\nd must be greather than 0");
        return -2;
    }


    // Initialize points in space
    if(init_points(&points) < 0) {
        printf("Could not initialize points\n");
        return -3;
    }


    // Allocate memory for tree nodes based on depth
    depth = 0;
    while((1 << depth) <= points.n)
        depth++;
    total_nodes = (1 << depth) - 1;
    printf("No of Points  = %d\nDimensions = %d\nVP-tree depth = %d\nVP-tree nodes = %d\n\n", points.n, points.d, depth, total_nodes);
    tree  = malloc(total_nodes * sizeof(tree_node));


    // Construct VP-tree
    gettimeofday(&start, NULL); // Start counting
    for(int i = 0; i < total_nodes; i++)
        tree[i].points_count = 0;
    vp_tree_construct(&points, tree, total_nodes);
    gettimeofday(&end, NULL);   // End counting
    elapsed_time = (end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec); // Elapsed time counted in us
    printf("VP construction time = %.3fms\n", (float)elapsed_time/1000); // Print time elapsed in milliseconds


    // All-KNN based on VP searching, with K = 2^(1:8)
    if(run_vp_based_knn) {
        float *neighbors_dist_array;
        int   *neighbors_idx;
        int    flag = 1, total_errors = 0, done = 0;
        int    k_pwr_low = 1, k_pwr_high = 8;
        gettimeofday(&start, NULL);
        for(int i = 0; i < points.n; i++) {
            for(int k_pwr = k_pwr_low; k_pwr <= k_pwr_high; k_pwr++) {
                int k = 1 << k_pwr;
                neighbors_dist_array = (float *)malloc(k * sizeof(float));
                neighbors_idx        = (int *)malloc(k * sizeof(int));
                vp_knn(tree, total_nodes, &points, &points.coords[i*points.d], k, neighbors_dist_array, neighbors_idx);
                if(!test_knn(tree, total_nodes, &points, &points.coords[i*points.d], k, neighbors_dist_array, neighbors_idx)) {
                    flag = 0;
                    total_errors++;
                    printf("An error was found running kNN on point with index = %d, with k = %d\n\n", i, k);
                }
            }
            done++;
            #ifdef KNN_PROGRESS
                printf("done = %d\n", done);
            #endif
        }
        gettimeofday(&end, NULL);
        elapsed_time = (end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec);
        printf("All-KNN time = %.3fms\n", (float)elapsed_time/1000);
        if(flag == 1)
            printf("All-kNN RAN SUCCESSFULLY\n");
        printf("Total correct   runs: %d\n", (k_pwr_high-k_pwr_low+1)*points.n - total_errors);
        printf("Total incorrect runs: %d\n", total_errors);
    }


    // Free memory
    free(points.coords);
    for(int i = 0; i < total_nodes; i++)
        if(tree[i].points_count > 0)
            free(tree[i].points_idx);
    free(tree);
}
