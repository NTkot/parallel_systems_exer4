#ifndef DATA_STRUCTS_H
#define DATA_STRUCTS_H

#include <stdint.h>

typedef struct {

    /**
     * @struct tree_node
     * @brief Structure containing info about a VP tree node
     * @var tree_node::vp_idx
     * Member 'vp_idx' contains the index of the vantage point in the current node
     * @var tree_node::points_idx
     * Member 'points_idx' contains the indices of the set of points belonging to the node (before separating them to inside and outside points)
     * @var tree_node::points_count
     * Member 'points_count' is the number of elements in the set of points belonging to the node
     * @var tree_node::median_dist
     * Member 'median_dist' is the median dist found between the vantage point and the rest of the points
     */

    int vp_idx;
    int *points_idx;
    int points_count;
    float median_dist;
} tree_node;

typedef struct {

    /**
     * @struct points_struct
     * @brief Structure holding information about points in space
     * @var points_struct::n
     * Member 'n' holds the number of points
     * @var points_struct::d
     * Member 'd' holds the number of dimensions
     * @var points_struct::coords
     * Member 'coords' holds the coordinates of all points
     * @var points_struct::dists
     * Member 'dists' is a condensed distance vector, that is meant to contain all the distances between all points (ONLY USED IF DYNAMIC_DISTANCES IS UNDEFINED)
     * @var points_struct::dist_calc
     * Member 'dist_calc' is a vector semaphore (same size as 'dists'), that each element is true(!0) if the corresponding distance in the 'dists' vector has been calculated (ONLY USED IF DYNAMIC_DISTANCES IS UNDEFINED)
     */

    int      n, d;
    float   *coords;
} points_struct;

#endif
