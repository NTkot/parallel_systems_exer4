#ifndef QUEUE_H
#define QUEUE_H

typedef struct {
    int *items;
    int maxsize;
    int front;
    int rear;
    int size;
} queue;

int init_queue(queue *q, int size);
void enqueue(queue *q, int x);
int dequeue(queue *q);

#endif