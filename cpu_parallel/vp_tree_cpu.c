#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "data_structs.h"
#include "vp_tree_cpu.h"
#include "queue.h"
#include "utils.h"

#define NUM_PTHREADS 3
#define DISTANCE_CALC_THREADS 4

pthread_mutex_t queue_lock, nodes_processed_lock;
pthread_cond_t  queue_not_empty;
pthread_barrier_t barr;
int total_nodes_processed;

void vp_tree_construct(points_struct *points, tree_node *tree, int total_nodes) {
    int n, d;
    n = points->n;
    d = points->d;
    pthread_t *threads;
    pthread_attr_t pthread_attr;
    worker_argument *worker_args;
    uint8_t construction_finished = 0;

    // Initialize process queue that dynamically assigns work to threads
    queue process_queue;
    init_queue(&process_queue, total_nodes/2 + 1);

    // Starting conditions (Initialize root node)
    tree[0].points_count = n;
    tree[0].points_idx   = malloc(n * sizeof(int));
    for(int i = 0; i < n; i++)
        tree[0].points_idx[i] = i;
    enqueue(&process_queue, 0);

    // POSIX-Threads variables needed for calling thread workers
    total_nodes_processed = 0;
    threads = (pthread_t *)malloc(NUM_PTHREADS * sizeof(pthread_t));
    pthread_attr_init(&pthread_attr);
    worker_args = (worker_argument *)malloc(NUM_PTHREADS * sizeof(worker_argument));
    pthread_mutex_init(&queue_lock, NULL);
    pthread_mutex_init(&nodes_processed_lock, NULL);
    pthread_cond_init(&queue_not_empty, NULL);
    pthread_barrier_init(&barr, NULL, NUM_PTHREADS);

    // Start parallel workers
    for(int i = 0; i < NUM_PTHREADS; i++) {
        worker_args[i].points        = points;
        worker_args[i].tree          = tree;
        worker_args[i].process_queue = &process_queue;
        worker_args[i].finished      = &construction_finished;
        worker_args[i].total_nodes   = total_nodes;
        pthread_create(&threads[i], &pthread_attr, thread_work, (void *)(&worker_args[i]));
    }
    
    // Wait for all threads to exit and join them
    for (int i = 0; i < NUM_PTHREADS; i++) {
        pthread_join(threads[i], NULL);
    }
}


void* thread_work(void *arg) {
    worker_argument *worker_arg = (worker_argument *)arg;
    int n                = worker_arg->points->n;
    int d                = worker_arg->points->d;
    float *coords        = worker_arg->points->coords;
    tree_node *tree      = worker_arg->tree;
    queue *process_queue = worker_arg->process_queue;
    uint8_t *finished    = worker_arg->finished;

    int i, current_n, current_vp;
    float *current_dists;

    while(1) {
        pthread_mutex_lock(&queue_lock);
        while(process_queue->size == 0) {   // Check process queue if it is not empty
            pthread_mutex_lock(&nodes_processed_lock);
            if(*finished == 1) {    // If we are finished (visited all nodes on tree array), then wake up all workers using pthread_cond_broadcast and exit function
                pthread_mutex_unlock(&nodes_processed_lock);
                pthread_mutex_unlock(&queue_lock);
                pthread_cond_broadcast(&queue_not_empty);
                pthread_exit(NULL);
            }
            pthread_mutex_unlock(&nodes_processed_lock);
            // If we are not finished and queue is empty, then enter wait state.
            // The thread unlocks queue_lock and will wake up again when a new node is entered in the process_queue (through pthread_cond_signal, see lines 140-141).
            pthread_cond_wait(&queue_not_empty, &queue_lock);   
        }
        i = dequeue(process_queue); // Dequeue tree node index ready for process from queue
        pthread_mutex_unlock(&queue_lock);

        // Get info about how many points are contained within the node
        current_n = tree[i].points_count;
        if(current_n > 0)
            tree[i].vp_idx = tree[i].points_idx[current_n - 1]; // Pick last point in set as the vantage point
        // If more than one points belong to the tree node, then we need to find find distances from VP and run quickselect
        if(current_n > 1) {
            // Construct current dists vector between points in node and vantage point
            current_vp     = tree[i].vp_idx;
            current_dists  = malloc(current_n * sizeof(float));

            // Construct current dists vector between points in node and vantage point
            #if DISTANCE_CALC_THREADS!=1
            #pragma omp parallel for num_threads(DISTANCE_CALC_THREADS) shared(current_dists, coords, tree)
            #endif
            for(int k = 0; k < current_n; k++) {
                float sum = 0;
                for(int l = 0; l < d; l++) {
                    sum += (coords[current_vp*d + l] - coords[tree[i].points_idx[k]*d + l]) *
                           (coords[current_vp*d + l] - coords[tree[i].points_idx[k]*d + l]);
                }
                current_dists[k] = sum;
            }

            // Quick-select
            tree[i].median_dist = kth_smallest(current_dists, tree[i].points_idx, 0, current_n-2, current_n/2 + (current_n % 2));

            // Write inside/outside point info to two childs
            // Left child
            tree[2*i+1].points_count = current_n/2;
            tree[2*i+1].points_idx   = malloc(tree[2*i+1].points_count * sizeof(int));
            memcpy(tree[2*i+1].points_idx, tree[i].points_idx, tree[2*i+1].points_count * sizeof(int));

            // Right child
            tree[2*i+2].points_count = current_n/2 - (1 - (current_n % 2));
            if(tree[2*i+2].points_count != 0) {
                tree[2*i+2].points_idx   = malloc(tree[2*i+2].points_count * sizeof(int));
                memcpy(tree[2*i+2].points_idx, tree[i].points_idx + current_n/2, tree[2*i+2].points_count * sizeof(int));
            }
        }
        // If node is not leaf, then add child nodes to the process queue
        if(i < worker_arg->total_nodes/2) {
            pthread_mutex_lock(&queue_lock);
            enqueue(process_queue, 2*i+1);
            enqueue(process_queue, 2*i+2);
            pthread_mutex_unlock(&queue_lock);
            pthread_cond_signal(&queue_not_empty);  // It's imperative to signal that the process queue is not empty TWICE
            pthread_cond_signal(&queue_not_empty);  // since we added TWO elements for process in the queue and pthread_cond_signal will wake >= 1 threads
        }

        // Update total nodes that were processed variable and check if we are finished
        pthread_mutex_lock(&nodes_processed_lock);
        total_nodes_processed++;
        if(total_nodes_processed == worker_arg->total_nodes)
            *finished = 1;
        pthread_mutex_unlock(&nodes_processed_lock);
    }
}


// If you want to find median from odd  number sized vector, k = size/2 + 1
// If you want to find median from even number sized vector, k = size/2 + 1 && k = size/2 and then median = average of these two
float kth_smallest(float *a, int *a_idx, int left, int right, int k) {
    int pivotIndex;
    while (left <= right) {
        pivotIndex = partition(a, a_idx, left, right);
 
        if (pivotIndex == (k - 1))
            return a[pivotIndex];
        else if (pivotIndex < (k - 1))
            left = pivotIndex + 1;
        else
            right = pivotIndex - 1;
    }
    return -1;
}


int partition(float *arr, int *arr_idx, int low, int high) {
    unsigned int i = low;
    float pivot = arr[high];
    for (unsigned int j = low; j < high; j++) {
        if(arr[j] < pivot) {
            swap_float(&arr[i], &arr[j]);
            swap_int(&arr_idx[i], &arr_idx[j]);
            i++;
        }
    }

    swap_float(&arr[i], &arr[high]);
    swap_int(&arr_idx[i], &arr_idx[high]);
    return i;
}