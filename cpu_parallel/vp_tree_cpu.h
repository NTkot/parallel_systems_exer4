#ifndef VP_TREE_SEQ_H
#define VP_TREE_SEQ_H

#include <stdint.h>
#include "data_structs.h"
#include "queue.h"

// Struct holding pthread worker arguments (for use in thread_work function)
typedef struct {
    points_struct *points;
    tree_node *tree;
    queue *process_queue;
    int total_nodes;
    uint8_t *finished;
} worker_argument;

// Construct VP-tree data structure using pthreads
void vp_tree_construct(points_struct *points, tree_node *tree, int total_nodes);

// Work of each worker to parallel construct VP-tree
void* thread_work(void *arg);

// Find kth_smallest element in array using quickselect
float kth_smallest(float *a, int *a_idx, int left, int right, int k);

// Function used for partitioning by kth_smallest function
int partition(float *arr, int *arr_idx, int low, int high);

#endif