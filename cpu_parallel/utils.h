#ifndef UTILS_H
#define UTILS_H

#include "data_structs.h"

// Initialize points_struct variable with random points
int init_points(points_struct *points);

// Memory swap float pointer values
void swap_float(float *a, float *b);

// Memory swap int pointer values
void swap_int(int *a, int *b);

// Print int vector in stdout
void print_int_vector(int *vec, int n);

// Print float vector in stdout
void print_float_vector(float *vec, int n);

// Print float 2D array in stdout
void print_2d_float_array(float *array, int n, int d);

#endif 