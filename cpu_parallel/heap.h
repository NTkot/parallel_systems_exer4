#ifndef HEAP_H
#define HEAP_H

typedef struct {
    int *array_idx;
    float *priorities;
    int size;
    int maxsize;
} heap;

int init_heap(heap *h, int size);
void heapify(heap *h, int i);
void insert_heap(heap *h, float new_priority, int new_idx);
void pop_heap(heap *h, float *priority, int *idx);
void noreturn_pop_heap(heap *h);
float current_max_heap(heap *h);

#endif