#include <stdio.h>
#include <stdlib.h>
#include "queue.h"


int init_queue(queue *q, int size) {
    q->items = (int*)malloc(size * sizeof(int));
    if(q->items == NULL)
        return 0;
    
    q->maxsize = size;
    q->front = 0;
    q->rear = -1;
    q->size = 0;
 
    return 1;
}


void enqueue(queue *q, int x) {
    if (q->size == q->maxsize) {
        printf("ERROR: Tried to enqueue object to full queue\nProgram Terminated\n");
        exit(EXIT_FAILURE);
    }

    q->rear = (q->rear + 1) % q->maxsize;
    q->items[q->rear] = x;
    q->size++;
}


int dequeue(queue *q) {
    int temp;
    if (q->size == 0) {
        printf("ERROR: Tried to dequeue object from empty queue\nProgram Terminated\n");
        exit(EXIT_FAILURE);
    }

    temp = q->items[q->front];
    q->front = (q->front + 1) % q->maxsize;
    q->size--;

    return temp;
}