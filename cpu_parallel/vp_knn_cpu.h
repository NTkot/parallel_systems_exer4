#ifndef VP_KNN
#define VP_KNN

#include "data_structs.h"
#include "heap.h"

// Function that runs a VP-tree-based KNN based on a tree (tree arg), that was constructed based on some points (points arg).
// The query point is given through the query_coords argument. The function returns the first K (k arg) neighbors in the form of
// their indices on the points array (neighbors_idx) and their corresponding distance squared from the query point (dist_array).
void vp_knn(const tree_node *tree, 
            const int total_nodes, 
            const points_struct *points, 
            const float *query_coords, 
            const int k, 
            float *dist_array, 
            int *neighbors_idx);

// Function that recursively searches the vantage-point tree and performs distance checks with the corresponding vantage-point in each node
// in order to perform VP-based KNN 
void search(const tree_node *tree, 
            const int total_nodes, 
            const int node_idx, 
            const points_struct *points, 
            const float *query_coords, 
            heap *neighbors, 
            float *tau);

// Function that checks the validity of the VP-based KNN algorithm. It compares the output of the VP-based KNN with a brute force KNN output
int test_knn(const tree_node *tree, 
             const int total_tree_nodes, 
             const points_struct *points, 
             const float *query_coords, 
             const int k, 
             const float *knn_dists, 
             const int *knn_idx);

#endif